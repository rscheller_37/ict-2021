package net.schoeninger.taskapp.services;

import net.schoeninger.taskapp.domain.Project;
import net.schoeninger.taskapp.repositories.ProjectRepository;
import net.schoeninger.taskapp.util.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project findById(long projectId) {
//        Optional<Project> projectOptional = projectRepository.findById(projectId);
//        if (projectOptional.isEmpty()) {
//            throw new EntityNotFoundException("");
//        }
//        return projectOptional.get();
        return projectRepository.findById(projectId)
                .orElseThrow(() -> new EntityNotFoundException("Couldn't find project for ID " + projectId));
    }

    @Override
    public Project addNew(Project project) {
        project.setId(null);
        return projectRepository.save(project);
    }

    @Override
    public Project updateExisting(long projectId, Project newValues) {
        Project project = findById(projectId);
        project.setTitle(newValues.getTitle());
        return projectRepository.save(project);
    }

    @Override
    public void deleteById(long projectId) {
        Project project = findById(projectId);
        projectRepository.delete(project);
    }

}
