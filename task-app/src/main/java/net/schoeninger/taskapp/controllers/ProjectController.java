package net.schoeninger.taskapp.controllers;

import net.schoeninger.taskapp.domain.Project;
import net.schoeninger.taskapp.services.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/projects")
public class ProjectController {

    private ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping
    public List<Project> getAll() {
        return projectService.findAll();
    }

    @GetMapping("/{projectId}")
    public Project getById(@PathVariable long projectId) {
        return projectService.findById(projectId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Project addNew(@RequestBody Project newProject) {
        return projectService.addNew(newProject);
    }

    @PutMapping("/{projectId}")
    public Project updateForId(@PathVariable long projectId, @RequestBody Project newValues) {
        return projectService.updateExisting(projectId, newValues);
    }

    @DeleteMapping("/{projectId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteForId(@PathVariable long projectId) {
        projectService.deleteById(projectId);
    }

}
