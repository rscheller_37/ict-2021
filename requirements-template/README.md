# Template for Lab 01

Markdown Cheatsheet: https://about.gitlab.com/handbook/markdown-guide

## Requirements Engineering

### Howto

1. Create the labels **User Story**, **Command**, **Query**.
2. Perform all the tasks up to section **Define APIs and Collaboration**.
3. Create a label for each identified service.
4. Perform the remaining tasks.

### Functional Requirements

https://www.atlassian.com/agile/project-management/user-stories

- [ ] [User Stories](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=User+Story)

### High-level Domain Model

- [ ] UML Class Diagram
- [ ] Textual description of the diagram

### System Operations

- [ ] [System Commands](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=Command)
- [ ] [System Queries](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=Query)

### Identify Services

- [ ] Identify Subdomains / Services
  - [PlaylistService](https://gitlab.com/fhs-schoeninger/ict-2021/-/issues?label_name%5B%5D=PlaylistService): Service for managing playlists (add, update, delete, add songs,...).

### Define APIs and Collaboration

- [ ] Assign system operations to services
- [ ] Define collaboration
- [ ] UML Component Diagram
- [ ] Textual description of the diagram
